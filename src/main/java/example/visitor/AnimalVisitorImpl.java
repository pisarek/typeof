package example.visitor;

import example.domain.Cat;
import example.domain.Dog;
import example.domain.Horse;

/**
 * @author Grzegorz Pisarek
 */
public class AnimalVisitorImpl<T> implements AnimalVisitor<String> {
    @Override
    public String visit(Dog a) {
        return "Dog";
    }

    @Override
    public String visit(Horse a) {
        return "Horse";
    }

    @Override
    public String visit(Cat cat) {
        return "Cat";
    }
}
