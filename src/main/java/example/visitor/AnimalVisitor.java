package example.visitor;

import example.domain.Cat;
import example.domain.Dog;
import example.domain.Horse;

/**
 * @author Grzegorz Pisarek
 */
public interface AnimalVisitor<T> {
    T visit(Dog a);
    T visit(Horse a);
    T visit(Cat cat);
}
