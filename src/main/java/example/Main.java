package example;

import example.domain.Animal;
import example.domain.Cat;
import example.domain.Dog;
import example.domain.Horse;
import example.visitor.AnimalVisitor;
import example.visitor.AnimalVisitorImpl;

import static com.nurkiewicz.typeof.TypeOf.whenTypeOf;

/**
 * @author Grzegorz Pisarek
 */
public class Main {
    public static void main(String[] args) {
        Animal animal = new Cat();

        whenTypeOf(animal)
                .is(Dog.class).then(dog -> System.out.println("Dog"))
                .is(Horse.class).then(horse -> System.out.println("Horse"))
                .orElse(a -> System.out.println("unknown"));

        AnimalVisitor<String> visitor = new AnimalVisitorImpl<>();
        System.out.println(animal.accept(visitor));
    }
}
