package example.domain;

import example.visitor.AnimalVisitor;

/**
 * @author Grzegorz Pisarek
 */
public interface Animal {
    <T> T accept(AnimalVisitor<T> visitor);
}
