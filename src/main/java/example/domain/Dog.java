package example.domain;

import example.visitor.AnimalVisitor;

/**
 * @author Grzegorz Pisarek
 */
public class Dog implements Animal {
    @Override
    public <T> T accept(AnimalVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
